<?php

namespace App\Http\Controllers;
use App\Models\usuarios;
use Illuminate\Http\Request;

class usuariosController extends Controller
{
    public function lista_usuarios(){
        $usuarios = usuarios::all();

        return $usuarios;
    }

    public function guarda_usuarios(Request $request){
        if($request->id == 0){
            $usuarios = new usuarios();
        }else{
            $usuarios = usuarios::find($request->id);
        }
        $usuarios->name = $request->name;
        $usuarios->ap_paterno = $request->ap_paterno;
        $usuarios->ap_materno = $request->ap_materno;
        $usuarios->email = $request->email;
        $usuarios->telefono = $request->telefono;
        $usuarios->direccion = $request->direccion;
        $usuarios->save();

        return response()->json($usuarios,200);
        }
        public function borrar_usuarios(Request $request){
            $usuarios = usuarios::find($request->id);
    
            $usuarios->delete();
            return response()->json("Eliminado",200);
        }
}
