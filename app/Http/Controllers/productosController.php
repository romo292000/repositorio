<?php

namespace App\Http\Controllers;

use App\Models\productos;
use Illuminate\Http\Request;

class productosController extends Controller
{
    public function lista_productos(Request $request){
        $productos = productos::all();

        return $productos;
       // return productos::where('descripcion','like',$request->palabra)->get();
    }

    public function guarda_producto(Request $request){

        if($request->id == 0){
            $producto = new productos();
        }else{
            $producto = productos::find($request->id);
        }
        $producto->codigo = $request->codigo;
        $producto->nombrepro = $request->nombrepro;
        $producto->precio = $request->precio;
        $producto->unidad_existencia = $request->unidad_existencia;
        $producto->save();

        return response()->json($producto,200);
    }

    public function borrar_producto(Request $request){
        $producto = productos::find($request->id);

        $producto->delete();
        return response()->json("Eliminado",200);
    }
    public function listar_productos_filtro(Request $request){
        $productos = Productos::where('codigo', 'like', $request->codigo)->get();
        return $productos;
    }
}
