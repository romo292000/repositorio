<?php

use App\Http\Controllers\almacenesController;
use App\Http\Controllers\productosController;
use App\Http\Controllers\clientesController;
use App\Http\Controllers\loginController;
use App\Http\Controllers\usuariosController;
use App\Http\Controllers\userController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("/lista_productos",[productosController::class,'listar_productos'])->middleware(('auth:api'));
Route::post("/listar_productos_filtro",[productosController::class,'listar_productos_filtro'])->middleware(('auth:api'));
Route::post("/login",[loginController::class,'login']);
Route::post("/logout",[loginController::class,'salir'])->middleware(('auth:api'));






Route::get('/lista_productos',[productosController::class,'lista_productos'])->middleware('auth:api');
Route::post('/guarda_producto',[productosController::class,'guarda_producto']);
Route::post('/borrar_producto',[productosController::class,'borrar_producto']);

Route::get('/lista_usuarios',[usuariosController::class,'lista_usuarios']);
Route::post('/guarda_usuarios',[usuariosController::class,'guarda_usuarios']);
Route::post('/borrar_usuarios',[usuariosController::class,'borrar_usuarios']);

Route::get('/lista_almacenes',[almacenesController::class,'lista_almacenes']);
Route::post('/guarda_almacenes',[almacenesController::class,'guarda_almacenes']);
Route::post('/borrar_almacenes',[almacenesController::class,'borrar_almacenes']);
//Route::get("/lista_user",[userController::class,'login']);
//Route::get("/borra_producto",[productosController::class,'borra_producto']);//->middleware(('auth:sanctum'));

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
